#include "formageometrica.hpp"
#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include <string>

using namespace std;

class Hexagono: public FormaGeometrica{
private:

public:
    Hexagono(string tipo, float base, float altura);
    ~Hexagono();
    float calcula_area();
    float calcula_perimetro();

};

#endif