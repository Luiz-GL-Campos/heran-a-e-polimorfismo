#include "formageometrica.hpp"
#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include <string>

using namespace std;

class Quadrado: public FormaGeometrica{
private:

public:
    Quadrado(string tipo, float base, float altura);
    ~Quadrado();
    float calcula_area();
    float calcula_perimetro();

};

#endif