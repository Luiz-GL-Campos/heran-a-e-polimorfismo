#include "pentagono.hpp"
#include <iostream>
#include <string>


Pentagono::Pentagono(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Pentagono::~Pentagono(){}

float Pentagono::calcula_area(){ //sobrescrita
    return ( (calcula_perimetro() * (get_altura()/2)) /2); // (get_altura()/2) usada como apotema
}
float Pentagono::calcula_perimetro(){ //sobrescrita
    return (5 * (get_base()));
}