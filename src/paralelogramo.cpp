#include "paralelogramo.hpp"
#include <iostream>
#include <string>


Paralelogramo::Paralelogramo(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
Paralelogramo::~Paralelogramo(){}

float Paralelogramo::calcula_area(){       
    return (get_base() * get_altura()); 
}
float Paralelogramo::calcula_perimetro(){  //sobrescrita
    return (2 * (get_base() + get_altura()));
}