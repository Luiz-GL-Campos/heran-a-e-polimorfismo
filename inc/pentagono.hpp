#include "formageometrica.hpp"
#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include <string>

using namespace std;

class Pentagono: public FormaGeometrica{
private:

public:
    Pentagono(string tipo, float base, float altura);
    ~Pentagono();
    float calcula_area();
    float calcula_perimetro();

};

#endif