#include "formageometrica.hpp"
#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include <string>

using namespace std;

class Triangulo: public FormaGeometrica{
private:

public:
    Triangulo(string tipo, float base, float altura);
    ~Triangulo();
    float calcula_area();
    float calcula_perimetro();
    
};

#endif